/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.subscribe;

import com.pelynch.cmefeed.CmeFeedUtils;
import com.pelynch.cmefeed.filter.FilterSecurityService;
import com.pelynch.cmefeed.onix.HandlerManager;
import com.pelynch.cmefeed.onix.PECmeMdHandler;
import com.pelynch.cmefeed.onix.PESecurityDefinitionService;
import com.pelynch.cmefeed.onix.tick.TickService;
import com.pelynch.cmefeed.pojo.PESecurityDefinition;
import com.pelynch.cmefeed.utils.DisplayOverride;
import com.pelynch.db.pojo.StockHeader;
import com.pelynch.utils.Utils;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author stefan
 */
@Component("subscribeRequestService")
public class SubscribeRequestManager implements SubscribeRequestService {

    private static Logger log = LoggerFactory.getLogger(SubscribeRequestManager.class);
    @Autowired(required = true)
    private PESecurityDefinitionService securityDefinitionService;

    @Autowired(required = true)
    private HandlerManager handlersManager;

    @Autowired(required = true)
    private TickService tickService;

    @Autowired(required = true)
    private CmeFeedUtils cmeFeedUtils;
    @Autowired(required = true)
    private DisplayOverride displayOverride;

    private Set<String> supportedExchanges;

    @Override
    public void subscribe(StockHeader stockHeader) {
        log.info("GETTING subscribe for " + stockHeader);
        String ticker = stockHeader.getTicker();
        //String symbol = cmeFeedUtils.parseSymbol(ticker);
        String exchString = stockHeader.getExchangeName();
        String exchangeSymbol = stockHeader.getExchTicker();

        String maturity = stockHeader.getMaturity();

        if (supportedExchanges != null && exchString != null && supportedExchanges.contains(exchString)) {
            if (maturity != null && exchangeSymbol != null) {

                String symbolToFind = exchangeSymbol + Utils.getMaturityCode(maturity);
                PESecurityDefinition securityDefinition = securityDefinitionService.getSecurityDefinition(symbolToFind);

               

                if (securityDefinition != null) {
                    Double cents = displayOverride.getDisplayOverride(exchangeSymbol);
                    if (cents != null) {
                        securityDefinition.setPeDisplayFactor(cents);
                        log.info("Updated because of custom display override: " + securityDefinition.toString());
                    }
                    processSecurityDefinition(ticker, securityDefinition);
                } else {
                    log.info("IGNORING SUBSCRIBE REQUEST FOR " + stockHeader + " DIDN'T FIND SECURITYDEFINITION");
                }
            } else {
                log.info("IGNORING SUBSCRIBE REQUEST FOR " + stockHeader + " INVALID MATURITY OR GLOBEX CODE");
            }

        } else {
            log.info("IGNORING SUBSCRIBE REQUEST FOR " + stockHeader + " NOT IN EXCHANGE LIST " + supportedExchanges);
        }

    }

    private void processSecurityDefinition(String ticker, PESecurityDefinition securityDefinition) {
        String channelKey = securityDefinition.getChannel();
        PECmeMdHandler handler = handlersManager.getHandler(channelKey);
        FilterSecurityService filterService = handler.getFilterSecurityService();
        int securityId = securityDefinition.getSecurityId();
        securityDefinitionService.addFullTicker(securityId, ticker);

        if (!filterService.isSubscribed(securityId)) {
            filterService.addToSubscribed(securityId);
        }
        tickService.createSubscriptionTick(securityId, ticker);
    }

//    public String[] getSupportedExchanges() {
//        return supportedExchanges;
//    }
    @Value("${feed.exchange}")
    public void parseExchanges(String[] supportedExchanges) {
        Set<String> set = new HashSet<String>();
        Collections.addAll(set, supportedExchanges);
        setSupportedExchanges(set);
    }

    public Set<String> getSupportedExchanges() {
        return supportedExchanges;
    }

    public void setSupportedExchanges(Set<String> supportedExchanges) {
        this.supportedExchanges = supportedExchanges;
    }

}
