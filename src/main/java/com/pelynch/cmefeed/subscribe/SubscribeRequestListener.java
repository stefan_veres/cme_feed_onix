/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.subscribe;

import com.pelynch.cmefeed.onix.ListenerFactory;
import com.pelynch.db.pojo.StockHeader;
import com.pelynch.jms.converter.SubscribeConverter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.stereotype.Component;
    import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
    
/**
 *
 * @author stefan
 */
@Component("subscribeRequestlistener")
public class SubscribeRequestListener implements MessageListener, Runnable {
    private static Logger log = LoggerFactory.getLogger(SubscribeRequestListener.class);
    
    private final BlockingQueue<Message> messages;

    @Autowired(required = true)
    private SubscribeConverter subscribeConverter;
    
   
    
    @Autowired(required = true) //@Resource(name="beanName") = @Autowired + @Qualifier("beanName")
    private SubscribeRequestService subscribeRequestService;

    
    
    
    public SubscribeRequestListener() {
        this.messages = new LinkedBlockingQueue<Message>();
    }
    

    @Override
    public void onMessage(Message msg) {

        try {
            this.messages.put(msg);
        } catch (InterruptedException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void run() {
        while (true) {
            Message msg;
            try {
                msg = messages.take();
                StockHeader stockHeader = (StockHeader) subscribeConverter.fromMessage(msg);
                subscribeRequestService.subscribe(stockHeader);
                
            } catch (InterruptedException ex ) {
                log.error(ex.getMessage());
            } catch ( JMSException ex) {
                log.error(ex.getMessage());
            } catch (MessageConversionException ex) {
                log.error(ex.getMessage());
            }
        }
    }
    
}
