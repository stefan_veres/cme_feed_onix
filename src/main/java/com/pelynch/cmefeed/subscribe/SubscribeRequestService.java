/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.subscribe;

import com.pelynch.db.pojo.StockHeader;

/**
 *
 * @author stefan
 */
public interface SubscribeRequestService {
    
    /**
     * Starts when stockHeader is ready.
     * From stock header, is taken symbol;
     * 
     * @param stockHeader
     */
    public void subscribe(StockHeader stockHeader);
    
    
    
    
}
