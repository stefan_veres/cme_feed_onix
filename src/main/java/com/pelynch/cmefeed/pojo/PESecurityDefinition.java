/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.pojo;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;

/**
 *
 * @author stefan
 */
//@ManagedResource(objectName = "bean:name=peSecurityDefinitionXY", description = "Onix managed bean")
public class PESecurityDefinition implements Serializable {//,CompositeData {
    private static final Logger log = LoggerFactory.getLogger(PESecurityDefinition.class);
    
    private String symbol;

    private Integer securityId;

    private String channel;

    private Double displayFactor;
    
    private volatile Double peDisplayFactor = 1d;

    public PESecurityDefinition() {
    }
    @ConstructorProperties({"symbol", "securityId", "displayFactor","channel"}) 
    public PESecurityDefinition(String symbol, Integer securityId, Double displayFactor, String channel) {
        this.symbol = symbol;
        this.securityId = securityId;
        this.channel = channel;
        this.displayFactor = displayFactor;
    }

    /**
     * @return the symbol
     */
      @ManagedAttribute(description="Symbol")
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return the securityId
     */
    
     @ManagedAttribute(description="securityId")
    public Integer getSecurityId() {
        return securityId;
    }

    /**
     * @param securityId the securityId to set
     */
    public void setSecurityId(Integer securityId) {
        this.securityId = securityId;
    }

    /**
     * @return the channelKey
     */
    @ManagedAttribute(description="Channel")
    public String getChannel() {
        return channel;
    }

    /**
     * @param channelKey the channelKey to set
     */
    public void setChannel(String channelKey) {
        this.channel = channelKey;
    }
    
    @ManagedAttribute(description="Display factor")
    public Double getDisplayFactor() {
        return displayFactor * peDisplayFactor;
    }

    public void setDisplayFactor(Double displayFactor) {
        this.displayFactor = displayFactor;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.symbol);
        hash = 47 * hash + Objects.hashCode(this.securityId);
        hash = 47 * hash + Objects.hashCode(this.channel);
        hash = 47 * hash + Objects.hashCode(this.displayFactor);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PESecurityDefinition other = (PESecurityDefinition) obj;
        if (!Objects.equals(this.symbol, other.symbol)) {
            return false;
        }
        if (!Objects.equals(this.securityId, other.securityId)) {
            return false;
        }
        if (!Objects.equals(this.channel, other.channel)) {
            return false;
        }
        if (!Objects.equals(this.displayFactor, other.displayFactor)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PESecurityDefinition{" + "symbol=" + symbol + ", securityId=" + securityId + ", channel=" + channel + ", displayFactor=" + displayFactor + ", peDisplayFactor=" + peDisplayFactor + '}';
    }

    public void setPeDisplayFactor(Double peDisplayFactor) {
        this.peDisplayFactor = peDisplayFactor;
    }

//    @Override
//    public CompositeType getCompositeType() {
//        CompositeType type = null;
//        try {
//            type  = new CompositeType("peSecurityDefinition", "peSecurityDefinition", new String[]{"symbol", "securityId","channel","displaFactor"}, new String[]{"symbol", "securityId","channel","displaFactor"}, new OpenType[]{SimpleType.STRING, SimpleType.INTEGER,SimpleType.STRING,SimpleType.DOUBLE});
//        } catch (OpenDataException ex) {
//            log.error(ex.toString());
//        }
//        
//        return type;
//    }
//
//    @Override
//    public Object get(String key) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

//    @Override
//    public Object[] getAll(String[] keys) {
//        return new Object[]{"symbol", "securityId","channel","displaFactor"};
//    }
//
//    @Override
//    public boolean containsKey(String key) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public boolean containsValue(Object value) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public Collection<?> values() {
//        return new Object[] {};
//    }

}
