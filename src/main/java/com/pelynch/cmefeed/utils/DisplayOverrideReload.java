/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.utils;

import com.pelynch.utils.Constants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

/**
 *
 * @author tom
 */
@Component
@ManagedResource(objectName = "bean:name=displayOverrideReload", description = "Onix managed bean")
public class DisplayOverrideReload implements MessageListener, DisplayOverride {

    private static Logger log = LoggerFactory.getLogger(DisplayOverrideReload.class);
    @Value("${display.override.file}")
    private String displaOverrideFile = "DisplayOverride.csv";

    @Value("${display.override.delimeter}")
    private String displaOverrideDelimeter = ",";

    private Map<String, Double> displayOverrides = null;

    @PostConstruct
    public void init() {
        reload();
    }

    public void onMessage(Message message) {
        if (message instanceof MapMessage) {
            try {
                processCommand((MapMessage) message);
            } catch (JMSException e) {
                log.error(e.getMessage());
            }
        }
    }

    private void processCommand(MapMessage msg) throws JMSException {
        String type = msg.getStringProperty(Constants.ATT_MESSAGE_TYPE);
        if (Constants.CMD_RELOAD_DISPLAY_OVERRIDES.equals(type)) {
            log.info("RELOADING DISPLAY OVERRIDES File");
            reload();
        }
    }

    @ManagedOperation(description = "Reload DisplayOverrideFile")
    public void reload() {
        displayOverrides = new HashMap<String, Double>();
        try {
            File file = openAndCheckFile(displaOverrideFile);
            displayOverrides = parseOverrides(file);
        } catch (IOException ex) {
            log.error(ex.toString());
        }

    }

    private File openAndCheckFile(String path) throws IOException {
        if (path == null) {
            throw new IOException("DisplayOverrideReload.reload() Trying to reload DisplayOverride but the path is NULL.");
        }

        // path = (path.startsWith("/")? attHome + path : attHome+"/"+path);
        log.info("Opening and parsing  " + path);
        File file = new File(path);

        if (!file.canRead()) {
            throw new IOException("DisplayOverrideReload.cannot read file '" + file);
        }

        return file;
    }

    private Map<String, Double> parseOverrides(File file) throws IllegalArgumentException {
        Map<String, Double> map = new HashMap<String, Double>();

        try {
            FileReader fr = new FileReader(file);
            BufferedReader in = new BufferedReader(fr);

            for (String line = in.readLine(); line != null; line = in.readLine()) {
                if (!line.isEmpty()) {
                    String symbol = parseSymbol(line);
                    Double override = parseOverride(line);
                    map.put(symbol, override);

                }

            }
            log.info("Parsed display override file and found " + map.size() + " lines");
            return map;

        } catch (FileNotFoundException ex) {
            log.error(ex.toString());
        } catch (IOException ex) {
            log.error(ex.toString());
        }

        return Collections.EMPTY_MAP;

    }

    public String parseSymbol(String line) {
        return line.split(displaOverrideDelimeter)[0];
    }

    public Double parseOverride(String line) {
        try {
            String overrideString = line.split(displaOverrideDelimeter)[1];
            Double override = Double.parseDouble(overrideString);

            return override;
        } catch (Exception ex) {
            log.error(ex.toString());
            return 1.0;
        }
    }

    /**
     * May return null!
     *
     * @param symbol
     * @return
     */
    @Override
    public Double getDisplayOverride(String symbol) {
        if (displayOverrides == null) {
            init();
        }
        Double override = displayOverrides.get(symbol);
        log.info("Got symbol " + symbol + " found display override " + override);
        return override;
    }

}
