/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.utils;

/**
 *
 * @author tom
 */
public interface DisplayOverride {
    /**
     * Expects universe.exchangeSymbol value (no maturity!)
     * @param symbol
     * @return 
     */
    public Double getDisplayOverride(String symbol);
}
