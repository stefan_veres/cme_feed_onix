/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.onix.tick;

import com.pelynch.cmefeed.jmx.LoggingMBean;
import com.pelynch.utils.Tick;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;
/**
 *
 * @author tom
 */

@Component
@ManagedResource(objectName="bean:name=tickQueueImpl", description="Onix managed bean")
public class TickQueueImpl implements TickQueue {
    private static final Logger log = LoggerFactory.getLogger(TickQueueImpl.class);
    private BlockingQueue<Tick> tickQ = new LinkedBlockingQueue<Tick>();
    private Tick lastTick;
    @Autowired
    private JmsTemplate jmsTemplate; 
    
    @Override
    public void add(Tick tick) {
        tickQ.add(tick);
        
    }
    
    @Override
    public Tick getLastTick() {
        return lastTick != null ? lastTick.clone(): null;
    
        
    }
    
    public Double getTickStamp() {
        return lastTick.getStamp();
    }

    @Override
    public void run() {
        while(true) {
            try {
                Tick tick = tickQ.take();
                lastTick = tick;
                //DateTime dateTime = new DateTime(DateTimeZone.forID("GB"));
               // tick.setStamp(new Double(dateTime.getSecondOfDay()));
                if (LoggingMBean.infoLogging) {
                    log.info("Sending tick "+tick.toString());
                }
                jmsTemplate.convertAndSend(tick);
            } catch (InterruptedException ex) {
                log.error(ex.toString());
            }
        }
    }

    public Queue<Tick> getTickQ() {
        return tickQ;
    }

    public void setTickQ(BlockingQueue<Tick> tickQ) {
        this.tickQ = tickQ;
    }
    
    @ManagedAttribute(description="Tick queue size")
    public int getSize() {
        return tickQ.size();
    }

    
    
    

    
    
}
