/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.onix.tick;

import biz.onixs.cme.md.handler.IPriceLevel;
import biz.onixs.util.ScaledDecimal;
import com.pelynch.utils.Tick;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tom
 */
public class TickDelta {

    private static final Logger log = LoggerFactory.getLogger(TickDelta.class);

    private final int bidQuantity;
    private final double bidPrice;
    private final int askQuantity;
    private final double askPrice;
    private final int tradeQuantity;
    private final double tradePrice;
    private final int TICK_TYPE;
    private final Long sendingTime;
    private final int securityId;

    public TickDelta(int securityId,IPriceLevel bestAsk, IPriceLevel bestBid, Long sendingTime) {
        this.securityId = securityId;
        this.askPrice = bestAsk != null ? safeScaledDecimal(bestAsk.getPrice()): 0;
//            this.askPrice = safeScaledDecimal(bestAsk.getPrice());
        this.askQuantity = bestAsk != null ? bestAsk.getQuantity() : 0;
//        this.bidPrice = safeScaledDecimal(bestBid.getPrice());
        this.bidPrice = bestBid != null ? safeScaledDecimal(bestBid.getPrice()): 0;
        this.bidQuantity = bestBid != null ? bestBid.getQuantity() : 0;
        this.tradePrice = 0;
        this.tradeQuantity = 0;
        this.TICK_TYPE = Tick.ATTBB_TICKTYPE_DUMMY;
        this.sendingTime = sendingTime;
    }

    public TickDelta(int securityId,ScaledDecimal tradePrice, int tradeVolume, Long sendingTime) {
        this.securityId = securityId;
        this.askPrice = 0;
        this.askQuantity = 0;
        this.bidPrice = 0;
        this.bidQuantity = 0;
        this.tradePrice = safeScaledDecimal(tradePrice);
        this.tradeQuantity = tradeVolume;
        this.TICK_TYPE = Tick.ATTBB_TICKTYPE_TRADE;
        this.sendingTime = sendingTime;
    }

    private static double safeScaledDecimal(ScaledDecimal value) {
        if (value != null) {
            BigDecimal bigDecimal = value.toBigDecimal();
            if (bigDecimal != null) {
                return bigDecimal.doubleValue();
            }
        }
        log.error("Unable to parse ScaledDecimal: " + value);
        return 0;
    }

    public static Logger getLog() {
        return log;
    }

    public int getBidQuantity() {
        return bidQuantity;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public int getAskQuantity() {
        return askQuantity;
    }

    public double getAskPrice() {
        return askPrice;
    }

    public int getTradeQuantity() {
        return tradeQuantity;
    }

    public double getTradePrice() {
        return tradePrice;
    }

    public int getTICK_TYPE() {
        return TICK_TYPE;
    }

    public Long getSendingTime() {
        return sendingTime;
    }

    public int getSecurityId() {
        return securityId;
    }

    @Override
    public String toString() {
        return "TickDelta{" + "bidQuantity=" + bidQuantity + ", bidPrice=" + bidPrice + ", askQuantity=" + askQuantity + ", askPrice=" + askPrice + ", tradeQuantity=" + tradeQuantity + ", tradePrice=" + tradePrice + ", TICK_TYPE=" + TICK_TYPE + ", sendingTime=" + sendingTime + ", securityId=" + securityId + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + this.bidQuantity;
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.bidPrice) ^ (Double.doubleToLongBits(this.bidPrice) >>> 32));
        hash = 43 * hash + this.askQuantity;
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.askPrice) ^ (Double.doubleToLongBits(this.askPrice) >>> 32));
        hash = 43 * hash + this.tradeQuantity;
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.tradePrice) ^ (Double.doubleToLongBits(this.tradePrice) >>> 32));
        hash = 43 * hash + this.TICK_TYPE;
        hash = 43 * hash + this.securityId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TickDelta other = (TickDelta) obj;
        if (this.bidQuantity != other.bidQuantity) {
            return false;
        }
        if (Double.doubleToLongBits(this.bidPrice) != Double.doubleToLongBits(other.bidPrice)) {
            return false;
        }
        if (this.askQuantity != other.askQuantity) {
            return false;
        }
        if (Double.doubleToLongBits(this.askPrice) != Double.doubleToLongBits(other.askPrice)) {
            return false;
        }
        if (this.tradeQuantity != other.tradeQuantity) {
            return false;
        }
        if (Double.doubleToLongBits(this.tradePrice) != Double.doubleToLongBits(other.tradePrice)) {
            return false;
        }
        if (this.TICK_TYPE != other.TICK_TYPE) {
            return false;
        }
        if (this.securityId != other.securityId) {
            return false;
        }
        return true;
    }
    
 
    
    
    
    

   

}
