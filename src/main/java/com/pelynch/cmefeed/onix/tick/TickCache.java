/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.onix.tick;

import com.pelynch.cmefeed.onix.PESecurityDefinitionService;
import com.pelynch.cmefeed.pojo.PESecurityDefinition;
import com.pelynch.cmefeed.utils.DisplayOverride;
import com.pelynch.types.TICK_SOURCE;
import com.pelynch.utils.Tick;
import com.pelynch.utils.Utils;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

/**
 *
 * @author tom
 */
@Component
@ManagedResource(objectName="bean:name=tickCache", description="Onix managed bean")

public class TickCache implements TickService,Runnable {

    private static final Logger log = LoggerFactory.getLogger(TickCache.class);

    private final Map<Integer, Tick> realTickMap = new ConcurrentHashMap<Integer, Tick>();
    private final Map<Integer, Tick> skeletonTickMap = new ConcurrentHashMap<Integer, Tick>();
    private final Map<Integer, TickDelta> tickDeltaCache = new ConcurrentHashMap<Integer, TickDelta>();

    private final DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyyMMdd");
    private final BlockingQueue<TickDelta> tickDeltaQ = new LinkedBlockingQueue<TickDelta>();
    
    
    public static boolean RUNNING=true;
    @Autowired
    private TickQueue tickQueue;

    @Autowired
    private PESecurityDefinitionService securityDefinitionService;
    @Autowired
    private Executor executor;
    
    @Autowired
    private DisplayOverride displayOverride;
    private AtomicInteger sequence = new AtomicInteger(0);
    private static long testL = 0l;
    private final double PRECISSION = 0.0000000001;
    
    
    @PostConstruct
    public void init() {
        log.info("INITIALIZING TICK CACHE");
        executor.execute(this);
    }
    
    @Override
    public void process(TickDelta tickDelta) {
        tickDeltaQ.add(tickDelta);
    }
    
    @ManagedAttribute(description="Tick delta queue size")
    public int getSize() {
        return tickDeltaQ.size();
    }
    
    @Override
    public void run() {
        log.info("STARTING TICK CACHE CONSUMER ");
        while (RUNNING) {
            try {
                TickDelta tickDelta = tickDeltaQ.take();
                processTickDelta(tickDelta);
                
            } catch (InterruptedException ex) {
               log.error("TICK CACHE CONSUMER ERROR "+ex);
            }
            
        }
    }
    
   
    private void processTickDelta(TickDelta tickDelta) {
        int securityId =tickDelta.getSecurityId();
        Tick tick = realTickMap.get(securityId);
        if (tick != null) {
            processRealTick(securityId, tickDelta, tick);
        } else {
            processSkeletonTick(securityId, tickDelta);
        }
    }
    
  

    private void processRealTick(Integer securityId, TickDelta tickDelta, Tick tick) {
        PESecurityDefinition definition= securityDefinitionService.getSecurityDefinition(securityId);
        Double displayFactor = definition.getDisplayFactor();
        Tick updatedTick = apply(tickDelta, tick,displayFactor);
        if (updatedTick.getType()!=Tick.ATTBB_TICKTYPE_DUMMY) {
            send(securityId,updatedTick);
            
        }
    }

    private void processSkeletonTick(Integer securityId, TickDelta tickDelta) {
        String ticker = securityDefinitionService.getFullTicker(securityId);
//       
        PESecurityDefinition definition= securityDefinitionService.getSecurityDefinition(securityId);
        Double displayFactor = definition.getDisplayFactor();
        Tick skeletonTick = getSkeletonTick(securityId, ticker);
        Tick updatedTick = apply(tickDelta, skeletonTick,displayFactor);

        if (validateTick(updatedTick, ticker)) {
            if (updatedTick.getType()!=Tick.ATTBB_TICKTYPE_DUMMY) {
                send(securityId,updatedTick);
                skeletonTickMap.remove(securityId);
            }
            //createSubscriptionTick(updateTick, ticker);
        } else {
            skeletonTickMap.put(securityId, updatedTick);
        }
    }
    
    private void send(Integer securityId,Tick updatedTick) {
        realTickMap.put(securityId, updatedTick);
        updatedTick.setSequence(sequence.getAndIncrement());
        tickQueue.add(updatedTick);
    }

    private Tick getSkeletonTick(Integer securityId, String ticker) {
        Tick tick = skeletonTickMap.get(securityId);
        if (tick == null) {
            tick = createSkeleton(ticker);
        }
        return tick;
    }

    @Override
    public void createSubscriptionTick(int securityId, String ticker) {
        Tick tick = realTickMap.get(securityId);
        if (tick == null) {
            log.info("Creating subscription tick " + securityId);
            createSubscriptionTick(createSkeleton(ticker));
        } else {
            createSubscriptionTick(tick.clone());
        }
    }

    private void createSubscriptionTick(Tick tick) {
        LocalDate date = new LocalDate();
        tick.setType(Tick.TICKTYPE_SUBSCRIPTION);
        tick.setDate(date.toDate());
        
        tickQueue.add(tick);
    }

   

    Tick apply(TickDelta tickDelta, final Tick masterTick,Double displayFactor) {
        Tick tick = masterTick.clone();

        //TODO refactor this for master tick and cloned tick
        int type = getTickType(tick, tickDelta,displayFactor);

        if (type == Tick.ATTBB_TICKTYPE_DUMMY) {
            //TODO probably send alert, and not send the tick
            log.error("Tick type is Dummy for delta: " + tickDelta.toString());
        }
        tick.setType(type);

        if (type==Tick.ATTBB_TICKTYPE_TRADE) {
                tick.setTradePrice(tickDelta.getTradePrice()*displayFactor);
                tick.setTradeSize(tickDelta.getTradeQuantity());
        }

        if (type==Tick.ATTBB_TICKTYPE_BID || type==Tick.ATTBB_TICKTYPE_ASK) {   
                double askPrice = tickDelta.getAskPrice() *displayFactor;
                double bidPrice = tickDelta.getBidPrice() *displayFactor;
                int askQuantity = tickDelta.getAskQuantity();
                int bidQuantity = tickDelta.getBidQuantity();
                
                
                if (askPrice!=0 && askQuantity!=0) {
                    tick.setAskPrice(askPrice);
                    tick.setAskSize(askQuantity);
                }
                
                if (bidPrice!=0 && bidQuantity!=0) {
                     tick.setBidPrice(bidPrice);
                     tick.setBidSize(bidQuantity);
                }
        }
        
//        if (type==Tick.ATTBB_TICKTYPE_TRADE && tick.getAskPrice()==0 && tick.getBidPrice()==0) {
//                tick.setAskPrice(tick.getTradePrice());
//                tick.setBidPrice(tick.getTradePrice());
//        }
        
       
        DateTime sendingTime= new DateTime(tickDelta.getSendingTime()/1000000,DateTimeZone.forID("GB"));
        tick.setStamp(new Double(sendingTime.getSecondOfDay()));
        return tick;
    }

    private Tick createSkeleton(String ticker) {
        Tick tick = new Tick();

        tick.setSecurity(ticker);
        tick.setSequence(sequence.get());

        tick.setBidPrice(0);
        tick.setAskPrice(0);
        tick.setTradePrice(0);

        tick.setBidSize(0);
        tick.setAskSize(0);
        tick.setTradeSize(0);

//      tick.setCondition(Integer.parseInt(tok.nextToken()));
        tick.setDate(Utils.getDate());
        tick.setTick_source(TICK_SOURCE.CME);

        return tick;
    }

    boolean validateTick(Tick tick, String ticker) {
        return tick.getSecurity().equals(ticker) &&
               tick.getType()!= Tick.ATTBB_TICKTYPE_DUMMY &&
               tick.getTradePrice() > 0 &&
               tick.getTradeSize() > 0 &&
               tick.getAskPrice() > 0 &&
               tick.getAskSize() > 0 &&
               tick.getBidPrice() > 0 &&
               tick.getBidSize() > 0;
                
//                (tick.getAskPrice() != -1.0 && tick.getAskSize() != -1.0
//                && tick.getBidPrice() != -1.0 && tick.getBidSize() != -1.0)
//                || tick.getTradePrice() != -1.0 && tick.getTradeSize() != -1.0;

    }

    int getTickType(Tick tick, TickDelta tickDelta,Double displayFactor) {
        int type = tickDelta.getTICK_TYPE();

        if (type != Tick.ATTBB_TICKTYPE_TRADE) {
            type = processAskBidType(tick, tickDelta,displayFactor);
        }

        return type;
    }

    private int processAskBidType(Tick tick, TickDelta tickDelta,Double displayFactor) {
        int type = tickDelta.getTICK_TYPE();

        if (tick.getAskPrice() == 0 && tick.getBidPrice() == 0) {
            log.warn("This is the skeleton tick. The AskPrice="+tick.getAskPrice()+" the bidPrice="+tick.getBidPrice()+" the tradePrice="+tick.getTradePrice());
            type = Tick.ATTBB_TICKTYPE_ASK;
        } else {
            try {
                boolean isAsk = isAskChange(tick, tickDelta,displayFactor);
                boolean isBid = isBidChange(tick, tickDelta,displayFactor);

                if (isAsk && !isBid) {
                    type = Tick.ATTBB_TICKTYPE_ASK;
                } else if (!isAsk && isBid) {
                    type = Tick.ATTBB_TICKTYPE_BID;
                } else {
                    if (isAsk && isBid) {
                        log.warn("It is both ask and bid for ticker "+tick.getSecurity()+ " " + tickDelta.toString());
                        type = Tick.ATTBB_TICKTYPE_ASK;
                    } else if (!isAsk && !isBid) {
                        log.warn("It is neither ask nor bid " + tickDelta.toString());
                        type = Tick.ATTBB_TICKTYPE_DUMMY;
                    }
                }

            } catch (Exception ex) {
                log.error("Error while checking Ask/Bid, setting type to Dummy " + ex.toString());
                type = Tick.ATTBB_TICKTYPE_DUMMY;
            }
        }

        return type;
    }

    private boolean isAskChange(Tick tick, TickDelta tickDelta,Double displayFactor) {
        return (tickDelta.getAskQuantity()!=0 && tickDelta.getAskPrice()!=0) && ((tick.getAskSize() != tickDelta.getAskQuantity() || (Math.abs(tick.getAskPrice() - tickDelta.getAskPrice()*displayFactor) > PRECISSION)));
    }

    private boolean isBidChange(Tick tick, TickDelta tickDelta,Double displayFactor) {
        return (tickDelta.getBidQuantity()!=0 && tickDelta.getBidQuantity()!=0) && ((tick.getBidSize() != tickDelta.getBidQuantity() || (Math.abs(tick.getBidPrice()-tickDelta.getBidPrice()*displayFactor) > PRECISSION)));
    }

    public TickQueue getTickQueue() {
        return tickQueue;
    }

    public void setTickQueue(TickQueue tickQueue) {
        this.tickQueue = tickQueue;
    }

    public PESecurityDefinitionService getSecurityDefinitionService() {
        return securityDefinitionService;
    }

    public void setSecurityDefinitionService(PESecurityDefinitionService securityDefinitionService) {
        this.securityDefinitionService = securityDefinitionService;
    }
    
    

   
    
   
    
    



}
