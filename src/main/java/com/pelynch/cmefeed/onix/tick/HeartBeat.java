/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.onix.tick;

import com.pelynch.types.TICK_SOURCE;
import com.pelynch.utils.Constants;
import com.pelynch.utils.Tick;
import com.pelynch.utils.Utils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author tom
 */

@Component
@ManagedResource(objectName="bean:name=hearbeat", description="Onix managed bean")
public class HeartBeat implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(HeartBeat.class);
    @Autowired
    private TickQueue tickQueue;
    
    private Tick lastHeartBeat;
    
    @Override
    @Scheduled(fixedDelay=1000)
    public void run() {
        
        Tick heartbeat= tickQueue.getLastTick();
        
        if (heartbeat==null) {
            heartbeat = new Tick();

            heartbeat.setSequence(0);

            heartbeat.setBidPrice(0);
            heartbeat.setAskPrice(0);
            heartbeat.setTradePrice(0);

            heartbeat.setBidSize(0);
            heartbeat.setAskSize(0);
            heartbeat.setTradeSize(0);

//      tick.setCondition(Integer.parseInt(tok.nextToken()));
            heartbeat.setDate(Utils.getDate());
            heartbeat.setTick_source(TICK_SOURCE.CME);
            
            
            
        }
        DateTime now = new DateTime();
        heartbeat.setStamp(now.getSecondOfDay());
        heartbeat.setSecurity(Constants.HEARTBEAT_TICKER);
        heartbeat.setIsHeartBeat(Boolean.TRUE);
        log.info("HEARTBEAT "+new DateTime()+" "+heartbeat);
        
        lastHeartBeat = heartbeat;
        tickQueue.add(heartbeat);
        
    }

    public TickQueue getTickQueue() {
        return tickQueue;
    }

    public void setTickQueue(TickQueue tickQueue) {
        this.tickQueue = tickQueue;
    }

    public Tick getLastHeartBeat() {
        return lastHeartBeat;
    }
    
    public void setLastHeartBeat(Tick lastHeartBeat) {
        this.lastHeartBeat = lastHeartBeat;
    }
    
    @ManagedAttribute(description="Last hearbeat stamp")
    public Double getHeartBeatTickStamp() {
        return lastHeartBeat.getStamp();
    }
    
    
    
    
    
}
