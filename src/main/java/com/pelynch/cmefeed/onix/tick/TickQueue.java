/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.onix.tick;

import com.pelynch.utils.Tick;

/**
 *
 * @author tom
 */
public interface TickQueue extends Runnable {

    public void add(Tick tick);
    
    public Tick getLastTick();
    
}
