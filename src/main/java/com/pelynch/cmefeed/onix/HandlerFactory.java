/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.onix;

import biz.onixs.cme.md.handler.BestBidAskTrackingParameter;
import biz.onixs.cme.md.handler.CmeMdHandler;
import biz.onixs.cme.md.handler.LicenseException;
import com.pelynch.cmefeed.filter.FilterSecurityManager;
import com.pelynch.cmefeed.filter.FilterSecurityService;
import com.pelynch.cmefeed.onix.tick.TickService;
import java.net.NetworkInterface;
import java.net.SocketException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
/**
 *
 * @author tom
 */
@Component
public class HandlerFactory {

    private static final Logger log = LoggerFactory.getLogger(HandlerFactory.class);

   

    @Value("${recoverSecurityDefinitionsOnGap}")
    private Boolean recoverSecurityDefinitionsOnGap;

    @Value("${useTcpReplay}")
    private boolean useTcpReplay;

    @Value("${useOneReceiverOnSamePort}")
    private boolean useOneReceiverOnSamePort;

    @Value("${useBFeeds}")
    private boolean useBFeeds;

    @Value("${playLog}")
    private boolean playLog;

    @Value("${localNetworkInterfaceA : \"\"}")
    private String localNetworkInterfaceNameA;

    @Value("${localNetworkInterfaceB : \"\"}")
    private String localNetworkInterfaceNameB;
    
    @Value("${tcpReplayUsername}")
    private String tcpReplayUsername="CME";
    
    @Value("${tcpReplayPassword}")
    private String tcpReplayPassword="CME";
    
    @Value("${maximumQueuedIncrementalRefreshMessages}")
    private Integer maximumQueuedIncrementalRefreshMessages=10000;

    

    @Autowired
    private ListenerFactory listenerFactory;

    @Autowired
    private PESecurityDefinitionService securityDefinitionService;
    
    @Autowired
    private TickService tickService;

    public PECmeMdHandler createHandler(String channelKey) {
        
        FilterSecurityService filterSecurityService = new FilterSecurityManager();
        CmeMdHandler cmeMdHandler = createCmeHandler(channelKey);
        PESecurityDefinitionListener listenerSD = this.listenerFactory.createPESecurityDefinitionListener(channelKey, securityDefinitionService);
        PEBussinessListener listenerUB = this.listenerFactory.createUniBussinessListener(filterSecurityService,securityDefinitionService,tickService);
            
        cmeMdHandler.registerSecurityDefinitionListener(listenerSD);
//            handler.registerSecurityStatusListener(listenerSD);
//            handler.registerStateChangedListener(listenerSD);
//        cmeMdHandler.registerRegularBookUpdatedListener(listenerUB);
        cmeMdHandler.registerTradeListener(listenerUB);
        cmeMdHandler.registerTopOfTheRegularBookUpdatedListener(listenerUB);
//        cmeMdHandler.registerStatisticsListener(listenerUB);
//        cmeMdHandler.registerRegularBookChangedListener(listenerUB);
        PECmeMdHandler ret = new PECmeMdHandler(cmeMdHandler,filterSecurityService);
                
        return ret;
    }
    private CmeMdHandler createCmeHandler(String channelKey) {
         CmeMdHandler handler = null;
         NetworkInterface localNetworkInterfaceA = null;
         NetworkInterface localNetworkInterfaceB = null;
        try {
           

            localNetworkInterfaceA = NetworkInterface.getByName(localNetworkInterfaceNameA);
            if (!"".equals(localNetworkInterfaceNameA) && localNetworkInterfaceA == null) {
                log.warn("Cannot find local network interface A: {}" + localNetworkInterfaceNameA);
            }
            localNetworkInterfaceB = NetworkInterface.getByName(localNetworkInterfaceNameB);
            if (!"".equals(localNetworkInterfaceNameB) && localNetworkInterfaceB == null) {
                log.warn("Cannot find local network interface B: {}" + localNetworkInterfaceNameB);
            }
        } catch (SocketException ex) {
            log.error(ex.getMessage());
        }
        try {

            handler = new CmeMdHandler(channelKey);

            handler.setChannelConfigurationFile("config/config.xml");
            handler.setTemplateFile("config/templates_FIXBinary.xml");

            if (useBFeeds) {
                handler.setUseIncrementalFeedB(true);
                    //handler.setUseSnapshotFeedB(true);
                //handler.setUseInstrumentReplayFeedB(true);
            }

            handler.setUseOneReceiverOnSamePort(useOneReceiverOnSamePort);

                //handler.addSubchannelFilter(1);
                // TEMP:
            //handler.setMaximumNumberOfQueuedIncrementalRefreshMessages(1000);
            //
            if (localNetworkInterfaceA != null) {
                handler.setLocalNetworkInterfaceA(localNetworkInterfaceA);
            }
            if (localNetworkInterfaceB != null) {
                handler.setLocalNetworkInterfaceB(localNetworkInterfaceB);
            }
            
            handler.setRecoverSecurityDefinitionsOnGap(recoverSecurityDefinitionsOnGap);
            handler.setTcpReplayUsername(tcpReplayUsername);
            handler.setTcpReplayPassword(tcpReplayPassword);
            handler.setTcpReplay(useTcpReplay);
            handler.getBestBidAskTrackingOptions().setBestBidAskTrackingParameters(BestBidAskTrackingParameter.ALL);
            CmeMdHandler.setMaximumNumberOfQueuedIncrementalRefreshMessages(maximumQueuedIncrementalRefreshMessages);

            
//            if (playLog) {
//                handler.registerLogReplayListener(listener);
//            }
        } catch (LicenseException ex) {
            log.error(ex.getMessage());
        }

        return handler;

    }

   
    public boolean isRecoverSecurityDefinitionsOnGap() {
        return recoverSecurityDefinitionsOnGap;
    }

    public void setRecoverSecurityDefinitionsOnGap(boolean recoverSecurityDefinitionsOnGap) {
        this.recoverSecurityDefinitionsOnGap = recoverSecurityDefinitionsOnGap;
    }

    public boolean isUseTcpReplay() {
        return useTcpReplay;
    }

    public void setUseTcpReplay(boolean useTcpReplay) {
        this.useTcpReplay = useTcpReplay;
    }

    public boolean isUseOneReceiverOnSamePort() {
        return useOneReceiverOnSamePort;
    }

    public void setUseOneReceiverOnSamePort(boolean useOneReceiverOnSamePort) {
        this.useOneReceiverOnSamePort = useOneReceiverOnSamePort;
    }

    public boolean isUseBFeeds() {
        return useBFeeds;
    }

    public void setUseBFeeds(boolean useBFeeds) {
        this.useBFeeds = useBFeeds;
    }

    public boolean isPlayLog() {
        return playLog;
    }

    public void setPlayLog(boolean playLog) {
        this.playLog = playLog;
    }

    public String getLocalNetworkInterfaceNameA() {
        return localNetworkInterfaceNameA;
    }

    public void setLocalNetworkInterfaceNameA(String localNetworkInterfaceNameA) {
        this.localNetworkInterfaceNameA = localNetworkInterfaceNameA;
    }

    public String getLocalNetworkInterfaceNameB() {
        return localNetworkInterfaceNameB;
    }

    public void setLocalNetworkInterfaceNameB(String localNetworkInterfaceNameB) {
        this.localNetworkInterfaceNameB = localNetworkInterfaceNameB;
    }


    public ListenerFactory getListenerFactory() {
        return listenerFactory;
    }

    public void setListenerFactory(ListenerFactory listenerFactory) {
        this.listenerFactory = listenerFactory;
    }

    public PESecurityDefinitionService getSecurityDefinitionService() {
        return securityDefinitionService;
    }

    public void setSecurityDefinitionService(PESecurityDefinitionService securityDefinitionService) {
        this.securityDefinitionService = securityDefinitionService;
    }

}
