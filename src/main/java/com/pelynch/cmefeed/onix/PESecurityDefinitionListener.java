/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.onix;

import biz.onixs.cme.md.handler.ISecurityDefinition;
import biz.onixs.cme.md.handler.event.HandlerStateChangedEventArgs;
import biz.onixs.cme.md.handler.event.HandlerStateChangedListener;
import biz.onixs.cme.md.handler.event.SecurityDefinitionEventArgs;
import biz.onixs.cme.md.handler.event.SecurityDefinitionListener;
import biz.onixs.cme.md.handler.event.SecurityStatusChangedEventArgs;
import biz.onixs.cme.md.handler.event.SecurityStatusChangedListener;
import biz.onixs.cme.md.handler.fix.Tag;
import biz.onixs.cme.sbe.FieldNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author stefan
 */
public class PESecurityDefinitionListener implements SecurityDefinitionListener, 
        SecurityStatusChangedListener,
        HandlerStateChangedListener
{
    
    private static final Logger log = LoggerFactory.getLogger(ListenerFactory.class);
    private final String channelKey;
    private final PESecurityDefinitionService securityDefinitionService;

    public PESecurityDefinitionListener(String channelKey, PESecurityDefinitionService securityDefinitions) {
        this.channelKey = channelKey;
        this.securityDefinitionService = securityDefinitions;
    }

    @Override
    public void onSecurityDefinition(SecurityDefinitionEventArgs sdea) {
        try {
            ISecurityDefinition definition = sdea.getDefinition();
            
            //log.info("mapping on channel "+channelKey+ " " + definition.getSymbol() + " to "+definition.getSecurityId() +" displayFactor "+definition.getDisplayFactor()+" unit of measure "+definition.getUnitOfMeasure()+" "+definition.getDefinition().getString(Tag.Currency)+" "+definition.getDefinition().toFixString());
            log.info("mapping on channel "+channelKey+ " " + definition.getSymbol() + " to "+definition.getSecurityId() +" displayFactor "+definition.getDisplayFactor()+" unit of measure "+definition.getUnitOfMeasure());
            //log.info("mapping on channel "+channelKey+ " " + definition.getSymbol() + " to "+definition.getSecurityId() +" displayFactor "+definition.getDisplayFactor()+" unit of measure "+definition.getUnitOfMeasure()+" Security group: "+definition.getSecurityGroup()+" "+definition.getDefinition().toFixString());
            securityDefinitionService.addSecurityDefinition(definition, channelKey);
            
        } catch (FieldNotFoundException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void onSecurityDefinitionRemoved(SecurityDefinitionEventArgs sdea) {
//        securityDefinitionService.(sdea.getDefinition(), handlerKey);
        log.info("onSecurityDefinitionRemoved :"+sdea.toString());
    
    }

    
    
    
    //Other useful methods:
    @Override
    public void onSecurityStatusChanged(SecurityStatusChangedEventArgs sscea) {
        //do on Security Status change. Find out when necessary, since 
        //we do not work with whole fix message, but only:
        //securityId, symbol and channel
        
        log.info("FEED :"+sscea.toString());
    }

    @Override
    public void onHandlerStateChanged(HandlerStateChangedEventArgs hscea) {
        log.info("FEED :"+hscea.toString());
    }
    
}
