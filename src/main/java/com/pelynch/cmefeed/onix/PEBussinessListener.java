/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.onix;

//21. (nr. in hand schema)
import biz.onixs.cme.md.handler.IOrderBook;
import biz.onixs.cme.md.handler.IPriceLevel;
import biz.onixs.cme.md.handler.event.ErrorEventArgs;
import biz.onixs.cme.md.handler.event.ErrorListener;
import biz.onixs.cme.md.handler.event.OrderBookChangedEventArgs;
import biz.onixs.cme.md.handler.event.OrderBookEventArgs;
import biz.onixs.cme.md.handler.event.OrderBookUpdatedEventArgs;
import biz.onixs.cme.md.handler.event.RegularBookChangedListener;
import biz.onixs.cme.md.handler.event.RegularBookUpdatedListener;
import biz.onixs.cme.md.handler.event.StatisticsEventArgs;
import biz.onixs.cme.md.handler.event.StatisticsListener;
import biz.onixs.cme.md.handler.event.StatisticsResetEventArgs;
import biz.onixs.cme.md.handler.event.TopOfTheOrderBookUpdatedEventArgs;
import biz.onixs.cme.md.handler.event.TopOfTheRegularBookUpdatedListener;
import biz.onixs.cme.md.handler.event.TradeEventArgs;
import biz.onixs.cme.md.handler.event.TradeListener;
import biz.onixs.cme.md.handler.event.*;
import biz.onixs.cme.sbe.FieldNotFoundException;
import biz.onixs.cme.sbe.FieldSet;
import com.pelynch.cmefeed.filter.FilterSecurityService;
import com.pelynch.cmefeed.jmx.LoggingMBean;
import com.pelynch.cmefeed.onix.tick.TickDelta;
import com.pelynch.cmefeed.onix.tick.TickService;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//21. (nr. in hand schema)
/**
 *
 * Implements all required onix business listeners. listens to actions of only
 * one CmeMdHandler.
 *
 *
 * @author stefan
 */
public class PEBussinessListener implements RegularBookUpdatedListener, RegularBookChangedListener,
        TradeListener, ErrorListener, TopOfTheRegularBookUpdatedListener, StatisticsListener {

    private static final Logger log = LoggerFactory.getLogger(PEBussinessListener.class);

    private FilterSecurityService filterSecurityService;
    private PESecurityDefinitionService peSecurityDefinitionService;
    private TickService tickService;

    public PEBussinessListener(FilterSecurityService filterSecurityService, PESecurityDefinitionService peSecurityDefinitionService, TickService tickService) {
        this.filterSecurityService = filterSecurityService;
        this.peSecurityDefinitionService = peSecurityDefinitionService;
        this.tickService = tickService;
    }
    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    @Override
    public void onTrade(TradeEventArgs tea) {

        int securityId = tea.getSecurityId();
        Long sendingTime = null;
        if (filterSecurityService.isSubscribed(securityId)) {
            try {
                sendingTime = tea.getSendingTime();
                //log.info(sendingTime.toString());
            } catch (FieldNotFoundException ex) {
                log.error("SENDING TIME EXCEPTION: " + ex.toString());
                sendingTime = new DateTime().getMillis();
            }
            TickDelta tickDelta = new TickDelta(securityId, tea.getPrice(), tea.getSize(), sendingTime);
            tickService.process(tickDelta);

            DateTime time = new DateTime(sendingTime / 1000000, DateTimeZone.forID("GB"));
            if (LoggingMBean.infoLogging) {
                log.info(securityId + " : " + time + " :" + tea.toString().replace("\n", "").replace("\r", ""));

            }

        }

    }

    @Override
    public void onTopOfTheRegularBookUpdated(TopOfTheOrderBookUpdatedEventArgs tb) {

        try {
            int securityId = tb.getSecurityId();
            IPriceLevel bestAsk = tb.getBestAsk();
            IPriceLevel bestBid = tb.getBestBid();
            if (bestAsk != null && bestBid != null) {
                Long sendingTime = null;
                if (filterSecurityService.isSubscribed(securityId)) {
                    try {
                        sendingTime = tb.getSendingTime();
                    } catch (FieldNotFoundException ex) {
                        log.error(ex.toString());
                        sendingTime = new DateTime().getMillis();
                    }

                    TickDelta tickDelta = new TickDelta(securityId, bestAsk, bestBid, sendingTime);
                    tickService.process(tickDelta);

                    DateTime time = new DateTime(sendingTime / 1000000, DateTimeZone.forID("GB"));
                    if (LoggingMBean.infoLogging) {
                        log.info(securityId + " : " + time + " :" + tb.toString().replace("\n", "").replace("\r", ""));

                    }

                }
            }
        } catch (Exception ex) {
            log.error(ex.toString());
        }

    }

    @Override
    public void onRegularBookUpdated(OrderBookUpdatedEventArgs obuea) {
        IOrderBook book = obuea.getBook();
        if (book != null) {
            int securityId = book.getSecurityId();
            //log.info("TRADEBOOK UPDATE: " + peSecurityDefinitionService.getSymbol(securityId));
            Long sendingTime = null;
            if (filterSecurityService.isSubscribed(securityId)) {
                sendingTime = obuea.getSendingTime();

                DateTime time = new DateTime(sendingTime / 1000000, DateTimeZone.forID("GB"));
                if (LoggingMBean.infoLogging) {
                    log.info(securityId + " : " + time + " :" + obuea.toString().replace("\n", "").replace("\r", ""));
                    log.info("BOOK UPDATED " + obuea.getBook().toDetailedString());
                }
            }
        }

    }

    @Override
    public void onRegularBookChanged(OrderBookChangedEventArgs obcea) {
        //obcea.getBook().getSecurityId()isSubscribed
        if (LoggingMBean.infoLogging) {
            log.info("BOOK CHANGED " + obcea.getBook().toDetailedString());
        }
    }

    @Override
    public void onRegularBookChangesReset(OrderBookEventArgs obea) {
        if (LoggingMBean.infoLogging) {
            log.info("BOOK CHANGED " + obea.getBook().toDetailedString());
        }
    }

    @Override
    public void onError(ErrorEventArgs eea) {
        if (LoggingMBean.infoLogging) {
            log.info(eea.toString());
        }
    }

    @Override
    public void onStatistics(StatisticsEventArgs sea) {
        if (LoggingMBean.infoLogging) {
            log.info("STATISTIC " + sea);
            List<FieldSet> x = sea.getDataBlocks().getItems();
            for (FieldSet fi : x) {
                Iterator<FieldSet> bla = x.iterator();
                while (bla.hasNext()) {
                    log.info("STATISTIC FIELD " + sea.getSecurityId() + " " + bla.next().toFixString('|'));
                }

            }

        }
    }

    @Override
    public void onStatisticsReset(StatisticsResetEventArgs srea) {
        if (LoggingMBean.infoLogging) {
            log.info("STATISTIC " + srea.toString());
        }
    }

}
