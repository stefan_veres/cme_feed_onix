/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.onix;

import biz.onixs.cme.md.handler.ISecurityDefinition;
import com.pelynch.cmefeed.pojo.PESecurityDefinition;

/**
 * Class for cashing data of PESecurityDefinitionService form CMExchange 
 gained from CmeMdHandlers in first – initialization phase.
 *
 * @author stefan
 */
public interface PESecurityDefinitionService {
    
    
 
    /**
     * Adds Security definition data into suitable form (the map).
     * 
     * @param definition security definition from CME Handler Listener
     * @param handlerKey number of the Handlers channel (i.e. CME channel on which the 
     *                   handler listens CME data).
     */
    public void addSecurityDefinition(ISecurityDefinition definition, String handlerKey);
    
    
    /**
     * gets suitable PESecurityDefinition entity for chosen symbol.
     * 
     * @param symbol
     * @return 
     */
    public PESecurityDefinition getSecurityDefinition(String symbol);
    
    /**
     *
     * @param securityId
     * @return
     */
    public PESecurityDefinition getSecurityDefinition(Integer securityId);
    
    
    
//    /**
//     * Gets channel nr from fix message.
//     * Reason: Listener doesn't know about Handlers channel.
//     * if we don't want to extract it via complicated way from Handler, 
//     * (or send it as attribute from handler to Listener and store it there as parameter)
//     * The easiest way is to extract it from Fix message.
//     * 
//     * @param fixS
//     * @return CME Channel number. 
//     */
//    public String getChannelNrFromFix(String fixS);

    public String getSymbol(int securityId);
    
    public String getFullTicker(int securityId);
    
    public void addFullTicker(int securityId, String ticker);
}
