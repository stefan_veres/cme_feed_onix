/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.onix;

import biz.onixs.cme.md.handler.ISecurityDefinition;
import biz.onixs.cme.sbe.FieldNotFoundException;
import biz.onixs.util.ScaledDecimal;
import com.pelynch.cmefeed.pojo.PESecurityDefinition;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

/**
 *
 * @author stefan
 */
@Component
@ManagedResource(objectName = "bean:name=peSecurityDefinitionCache", description = "Onix managed bean")
public class PESecurityDefinitionCache implements PESecurityDefinitionService {

    private static final Logger log = LoggerFactory.getLogger(PESecurityDefinitionCache.class);
    /**
     * Map for storage pairs uid : PESecurityDefinition. uid means the unique
     * combination that can be exctracted from StockHeader. For the time being
     * it is equivalent to 'symbol' from ISecurityDefinition (e.g. "ESZ5-ESH6").
     * Multiplicity of uid - PESecurityDefinition is 1 : 1.
     */
    private final Map<String, PESecurityDefinition> peSecurityDefinitionMap = new ConcurrentHashMap<String, PESecurityDefinition>();
    private final Map<Integer, PESecurityDefinition> peSecurityDefinitionById = new ConcurrentHashMap<Integer, PESecurityDefinition>();
    private final Map<Integer, String> idToSymbolMap = new ConcurrentHashMap<Integer, String>();
    private final Map<Integer, String> idToFullTickerMap = new ConcurrentHashMap<Integer, String>();

  
    @Override
    public void addSecurityDefinition(ISecurityDefinition cmeSecurityDefinition, String channelKey) {

        try {
            String symbol = cmeSecurityDefinition.getSymbol();
            Double displayFactor = null;
            try {
                ScaledDecimal x = cmeSecurityDefinition.getDisplayFactor();
                displayFactor = x.toBigDecimal().doubleValue();
            } catch (Exception ex) {
                log.error(ex.toString());
            }
            Integer securityId = cmeSecurityDefinition.getSecurityId();
            PESecurityDefinition peSecurityDefinition = new PESecurityDefinition(symbol, securityId, displayFactor, channelKey);
            this.peSecurityDefinitionMap.put(symbol, peSecurityDefinition);
            this.peSecurityDefinitionById.put(securityId, peSecurityDefinition);
            this.idToSymbolMap.put(securityId, symbol);

        } catch (FieldNotFoundException ex) {
            log.error(ex.toString());
        }
    }

    @Override
    public PESecurityDefinition getSecurityDefinition(String symbol) {
        return this.peSecurityDefinitionMap.get(symbol);
    }

    @Override
    public PESecurityDefinition getSecurityDefinition(Integer securityId) {
        return this.peSecurityDefinitionById.get(securityId);
    }

    @Override
    public String getSymbol(int securityId) {
        return idToSymbolMap.get(securityId);
    }

    @Override
    public String getFullTicker(int securityId) {
        return idToFullTickerMap.get(securityId);
    }

    @Override
    public void addFullTicker(int securityId, String ticker) {
        idToFullTickerMap.put(securityId, ticker);
    }

    @ManagedAttribute(description = "Security Definition by symbol map")
    public Map<String, PESecurityDefinition> getPeSecurityDefinitionMap() {
        return peSecurityDefinitionMap;
    }
    
   

    @ManagedAttribute(description = "Security Definition by security id map")
    public Map<Integer, PESecurityDefinition> getPeSecurityDefinitionById() {
        return peSecurityDefinitionById;
    }

    @ManagedAttribute(description = "Security Id to Symbol map")
    public Map<Integer, String> getIdToSymbolMap() {
        return idToSymbolMap;
    }

    @ManagedAttribute(description = "Full Ticker to Security Id map")
    public Map<Integer, String> getIdToFullTickerMap() {
        return idToFullTickerMap;
    }

}
