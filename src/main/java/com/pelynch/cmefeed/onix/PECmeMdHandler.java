/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.onix;

import biz.onixs.cme.md.handler.CmeMdHandler;
import biz.onixs.cme.md.handler.HandlerStartupStrategy;
import com.pelynch.cmefeed.filter.FilterSecurityService;

/**
 *
 * @author tom
 */
public class PECmeMdHandler {
    private CmeMdHandler cmeMdHandler;
    private FilterSecurityService filterSecurityService;

    public PECmeMdHandler(CmeMdHandler cmeMdHandler,FilterSecurityService filterSecurityService) {
        this.cmeMdHandler = cmeMdHandler;
        this.filterSecurityService = filterSecurityService;
    }
    
    public void start(HandlerStartupStrategy mode, boolean useSecurityDefinitionCache) {
        cmeMdHandler.start(mode,useSecurityDefinitionCache);
    }

    public CmeMdHandler getCmeMdHandler() {
        return cmeMdHandler;
    }

    public void setCmeMdHandler(CmeMdHandler cmeMdHandler) {
        this.cmeMdHandler = cmeMdHandler;
    }

    public FilterSecurityService getFilterSecurityService() {
        return filterSecurityService;
    }

    public void setFilterSecurityService(FilterSecurityService filterSecurityService) {
        this.filterSecurityService = filterSecurityService;
    }
    
    
    
}
