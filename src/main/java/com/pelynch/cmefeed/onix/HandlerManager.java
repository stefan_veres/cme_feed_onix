/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.onix;

import biz.onixs.cme.md.handler.HandlerStartupStrategy;
import com.pelynch.cmefeed.onix.tick.HeartBeat;
import com.pelynch.cmefeed.onix.tick.TickQueue;
import com.pelynch.cmefeed.subscribe.SubscribeRequestListener;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author tom
 */
@Component("handlerManager")
public class HandlerManager {
   
    private static final Logger log = LoggerFactory.getLogger(HandlerManager.class);
   
    private String[] channels = {"310", "312", "314", "316", "318", "320", "340", "342",
        "344", "346", "360",  "382", "384", "386", "410",
        };
    
//    private String[] channels = {"310"};
    private final Map<String, PECmeMdHandler> handlersMap = new ConcurrentHashMap<String, PECmeMdHandler>();
    
    @Autowired
    private HandlerFactory handlerFactory;
    
   
    @Value("${mode}")
    private String stringMode;
    @Value("${useSecurityDefinitionCache}")
    private boolean useSecurityDefinitionCache;
    
    @Autowired(required = true)
    private Executor executor;
    
    @Autowired(required = true)
    private SubscribeRequestListener subscribeRequestlistener;
    
    @Autowired(required = true)
    private TickQueue tickQueue;
    
    @Autowired
    private HeartBeat heartBeat;
    
    
    
    
    @PostConstruct
    public void init() {
        
        for (String channelKey : channels) {
            PECmeMdHandler handler = handlerFactory.createHandler(channelKey);
            HandlerStartupStrategy mode = HandlerStartupStrategy.valueOf(stringMode);
            handlersMap.put(channelKey, handler);
            
            handler.start(mode, useSecurityDefinitionCache);
            
            
        }
        
        if(executor!=null){
            log.info("Starting async message listener");
            executor.execute(subscribeRequestlistener);
            executor.execute(tickQueue);
           
        }
        
    }

    public PECmeMdHandler getHandler(String channelKey) {
        return handlersMap.get(channelKey);
    }
    
   
   
    public String[] getChannels() {
        return channels;
    }
    @Value("${feed.channels}")
    public void setChannels(String[] channels) {
        this.channels = channels;
    }
    
    
    
    

    
}
