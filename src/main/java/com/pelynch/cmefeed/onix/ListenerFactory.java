/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.onix;

import com.pelynch.cmefeed.filter.FilterSecurityService;
import com.pelynch.cmefeed.onix.tick.TickService;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author tom
 */
@Component
public class ListenerFactory {
    private static final Logger log = LoggerFactory.getLogger(ListenerFactory.class);
    
    public PESecurityDefinitionListener createPESecurityDefinitionListener (String channelKey,PESecurityDefinitionService securityDefinitionService) {
         return new PESecurityDefinitionListener(channelKey, securityDefinitionService);
    }

    public PEBussinessListener createUniBussinessListener(FilterSecurityService filterSecurityService,PESecurityDefinitionService securityDefinitionService,TickService tickService) {
        PEBussinessListener uniBussinessListener=new PEBussinessListener(filterSecurityService,securityDefinitionService,tickService);
        
        return uniBussinessListener;
    }
}
