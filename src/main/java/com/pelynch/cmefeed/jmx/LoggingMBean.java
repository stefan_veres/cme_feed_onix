/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.jmx;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

/**
 *
 * @author tom
 */
@Component
@ManagedResource(objectName="bean:name=loggingMBean", description="Onix managed bean")
public class LoggingMBean {
    public static boolean infoLogging = true;

     @ManagedAttribute(description="info log turn on/off property")
    public static boolean isInfoLogging() {
        return infoLogging;
    }
    
    @ManagedAttribute(description="set info log turn on/off property")
    public static void setInfoLogging(boolean infoLogging) {
        LoggingMBean.infoLogging = infoLogging;
    }
    
    
    
}
