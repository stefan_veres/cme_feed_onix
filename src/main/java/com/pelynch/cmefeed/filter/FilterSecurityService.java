/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.filter;

import com.pelynch.cmefeed.pojo.PESecurityDefinition;

/**
 *
 * @author tom
 */
public interface FilterSecurityService {
   public void addToSubscribed(int securityId);
   public boolean isSubscribed(int securityId);
   
}   
