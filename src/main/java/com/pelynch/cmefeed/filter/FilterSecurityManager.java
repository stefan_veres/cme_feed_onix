/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed.filter;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

/**
 *
 * @author tom
 */
@Component
@ManagedResource(objectName="bean:name=filterSecurityManager", description="Onix managed bean")
public class FilterSecurityManager implements FilterSecurityService { 
    
    private Set<Integer> subscribedSymbols = new CopyOnWriteArraySet<Integer>();
    
    @Override
    public void addToSubscribed(int securityId) {
        subscribedSymbols.add(securityId);
        
    }

    @Override
    public boolean isSubscribed(int securityId) {
        return subscribedSymbols.contains(securityId);
    }
    
    @ManagedAttribute(description="Subscribed symbols")
    public Set<Integer> getSubscribedSymbols() {
        return subscribedSymbols;
    }
    
    
    
    
    
}
