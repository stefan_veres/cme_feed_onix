/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed;

/**
 * Instead of xml config file
 *
 * @author stefan
 */
import com.pelynch.cmefeed.subscribe.SubscribeRequestListener;
import com.pelynch.jms.converter.TickConverter;
import com.pelynch.jms.spring.SimpleListenerContainer;
import com.pelynch.tick.TickUtils;
import java.util.concurrent.Executor;
import javax.jms.ConnectionFactory;
import javax.jms.Topic;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableMBeanExport
@EnableScheduling
@PropertySource("classpath:cmeFeed.properties")
@ComponentScan(basePackages = {"com.pelynch.cmefeed", "com.pelynch.jms.converter"})
public class CmeFeedConfiguration {

    @Value("${topic.TICK_FEED}")
    private String subscribeTopic;

    @Value("${topic.TICKS}")
    private String ticksTopic;

    @Value("${jms.broker}")
    private String jmsBrokerURL;

    @Autowired(required = true)
    @Qualifier("subscribeRequestlistener")
    private SubscribeRequestListener subscribeRequestListener;

    @Bean//(name = "tickFeedTopic")
    public Topic subscribeTopic() {
        return new ActiveMQTopic(subscribeTopic);
    }

    @Bean//(name = "tickFeedTopic")
    public Topic ticksTopic() {
        ActiveMQTopic topic = new ActiveMQTopic(ticksTopic);
        return topic;
    }

//
    @Bean//(name = "connectionFactory")
    public ConnectionFactory connectionFactory() {
        ActiveMQConnectionFactory ret = new ActiveMQConnectionFactory(jmsBrokerURL);
        CachingConnectionFactory caching = new CachingConnectionFactory(ret);
        return caching;
    }

    @Bean//(name = "tickFeedContainer")
    public SimpleListenerContainer simpleListenerContainer() {

        SimpleListenerContainer cont = new SimpleListenerContainer();
        cont.setConnectionFactory(connectionFactory()); //        <property name="connectionFactory" ref="cachingConnectionFactory"/>
        cont.setDestination(subscribeTopic()); //        <property name="destination" ref="inFixTopic"/>
        cont.setMessageListener(subscribeRequestListener); //        <property name="messageListener" ref="inFixListener"/>

        return cont;

    }

    @Bean
    public JmsTemplate jmsTemplate() {
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setDefaultDestination(ticksTopic());
        jmsTemplate.setMessageConverter(tickConverter());

        jmsTemplate.setConnectionFactory(connectionFactory());

        jmsTemplate.setDeliveryPersistent(false);
        jmsTemplate.setTimeToLive(500);

        return jmsTemplate;
    }

    @Bean
    public TickUtils tickUtils() {
        return new TickUtils();

    }

    @Bean
    public MessageConverter tickConverter() {
        TickConverter converter = new TickConverter();
        converter.setTickUtils(tickUtils());
        return converter;
    }

//    @Bean
//    public MessageConverter subscribeConverter() {
//        SubscribeConverter converter = new SubscribeConverter();
//        converter.setExchangeService(createExchangeService());
//        return converter;
//    }
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public Executor executor() {
        return new SimpleAsyncTaskExecutor();
    }

//    @Bean
//    public MetadataMBeanInfoAssembler assembler() {
//        MetadataMBeanInfoAssembler assembler = new MetadataMBeanInfoAssembler();
//        assembler.setAttributeSource(annotationJmxAttributeSource());
//
//        return assembler;
//    }
//
//    @Bean
//    public AnnotationJmxAttributeSource annotationJmxAttributeSource() {
//        AnnotationJmxAttributeSource annotationJmxAttributeSource = new AnnotationJmxAttributeSource();
//        return annotationJmxAttributeSource;
//    }
//
//    @Bean
//    public MetadataNamingStrategy namingStrategy() {
//        MetadataNamingStrategy metadataNamingStrategy = new MetadataNamingStrategy();
//        metadataNamingStrategy.setAttributeSource(annotationJmxAttributeSource());
//
//        return metadataNamingStrategy;
//    }
//  
//  @Bean
//  public JmxTestBean jmxTestBean() {
//      JmxTestBean jmxTestBean = new JmxTestBean();
//      jmxTestBean.setName("TEST");
//      jmxTestBean.setAge(100);
//      
//      return jmxTestBean;
//  }

//    @Bean
//    public MBeanExporter exporter() {
////        Map<String,Object> map = new HashMap<String,Object>();
////        map.put("bean:name=testBean1", jmxTestBean());
//
//        MBeanExporter exporter = new MBeanExporter();
//        exporter.setAssembler(assembler());
//        exporter.setNamingStrategy(namingStrategy());
//        exporter.setAutodetect(true);
////        exporter.setBeans(map);
//
//        return exporter;
//    }

}
