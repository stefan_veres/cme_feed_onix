/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author tom
 */
@Component
public class CmeFeedUtils {
     private static final Logger log = LoggerFactory.getLogger(CmeFeedUtils.class);
    public String parseSymbol(String symbol) {
        String ret = null;
        if (symbol==null) {
            log.error("Symbol couldn't bee null");
            return null;
        } else {
            int lastIndex = symbol.trim().lastIndexOf(" ");
            if (lastIndex>0) {
                ret = symbol.substring(0,lastIndex).trim();
            } else {
                log.error("Symbol is invalid:"+symbol);
            }
        }
        return ret;
        
    }
}
