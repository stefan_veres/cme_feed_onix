/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.listeners;

import biz.onixs.cme.md.handler.IPriceLevel;
import biz.onixs.cme.md.handler.event.TopOfTheOrderBookUpdatedEventArgs;
import biz.onixs.cme.sbe.FieldNotFoundException;
import biz.onixs.util.ScaledDecimal;
import com.pelynch.cmefeed.MainTest;
import com.pelynch.cmefeed.filter.FilterSecurityService;
import com.pelynch.cmefeed.onix.PEBussinessListener;
import com.pelynch.cmefeed.onix.PESecurityDefinitionService;
import com.pelynch.cmefeed.onix.tick.TickDelta;
import com.pelynch.cmefeed.onix.tick.TickService;
import java.math.BigDecimal;
import java.util.logging.Level;
//import com.pelynch.cmefeed.configuration.ComponentFactory;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author stefan
 */
//please check, whether spring need to be used:
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={MainTest.class})
public class PEBussinessListenerTest extends TestCase {
  
    private Logger log = Logger.getLogger(PEBussinessListenerTest.class);

    @Test
    public void tickDeltaTest() {

         int securityId = 100;
         IPriceLevel askPriceLevelMock =  EasyMock.createMock(IPriceLevel.class);
         IPriceLevel bidPriceLevelMock =  EasyMock.createMock(IPriceLevel.class);
         EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(10000)).times(2);
         EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(20000)).times(2);
         EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(250))).times(2);
         EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(500))).times(2);
         
         EasyMock.replay(askPriceLevelMock);
         EasyMock.replay(bidPriceLevelMock);
         
         IPriceLevel askPriceLevel = null;
         IPriceLevel bidPriceLevel = null;
         Long sendingTime = System.currentTimeMillis();
        
         TickDelta tickDelta1 = new TickDelta(securityId, askPriceLevel, bidPriceLevel, sendingTime);
         TickDelta tickDelta2 = new TickDelta(securityId, askPriceLevelMock, bidPriceLevelMock, sendingTime);
         TickDelta tickDelta3 = new TickDelta(securityId, askPriceLevelMock, bidPriceLevel, sendingTime);
         TickDelta tickDelta4 = new TickDelta(securityId, askPriceLevel, bidPriceLevelMock, sendingTime);

         assertTrue(true);
    }

    

    
}
