/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pelynch.cmefeed.onix.tick;

import biz.onixs.cme.md.handler.IPriceLevel;
import biz.onixs.util.ScaledDecimal;
import com.pelynch.cmefeed.MainTest;
import com.pelynch.types.TICK_SOURCE;
import com.pelynch.utils.Tick;
import com.pelynch.utils.Utils;
import java.math.BigDecimal;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author stefan
 */
//please check, whether spring need to be used:
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={MainTest.class})
public class TickCacheTest extends TestCase {
  
    private Logger log = Logger.getLogger(TickCacheTest.class);
    private Tick creatTick(String security,int seq,double bidPrice,double askPrice,double tradePrice,int bidSize,int askSize,int tradeSize,int date,TICK_SOURCE source) {
        Tick tick = new Tick();

        tick.setSecurity(security);
        tick.setSequence(seq);

        tick.setBidPrice(bidPrice);
        tick.setAskPrice(askPrice);
        tick.setTradePrice(tradePrice);

        tick.setBidSize(bidSize);
        tick.setAskSize(askSize);
        tick.setTradeSize(tradeSize);

//      tick.setCondition(Integer.parseInt(tok.nextToken()));
        tick.setDate(date);
        tick.setTick_source(source);
 
        return tick;
    }
    @Test
    public void skeletonTickTypeTest() {
        int securityId=1000; 
        Double displayFactor = 0.001;
         
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, null, null, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 0, 0, 0, 0, 0, 0,0, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_ASK);
    }
    
    @Test
    public void skeletonAskTickTypeTest() {
        int securityId=1000; 
        Double displayFactor = 0.001;
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, askPriceLevelMock, null, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 0, 0, 0, 0, 0, 0,0, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_ASK);
    }
    
    
     @Test
    public void skeletonBidTickTypeTest() {
        int securityId=1000; 
        Double displayFactor = 0.001;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        
        EasyMock.replay(bidPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, null, bidPriceLevelMock, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 0, 0, 0, 0, 0, 0,0, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_ASK);
    }
    
    
      
    @Test
    public void skeletonBothTickTypeTest() {
        int securityId=1000; 
        Double displayFactor = 0.001;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(237.8)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(5000));
        
        EasyMock.replay(bidPriceLevelMock);
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, askPriceLevelMock, bidPriceLevelMock, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 0, 0, 0, 0, 0, 0,0, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_ASK);
    }
       
    
    
    @Test
    public void realTickDummyTypeTest() {
        int securityId=1000;
        Double displayFactor = 0.001;
        
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, null, null, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_DUMMY);
    }
    
    @Test
    public void realTickAskTypeTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,askPriceLevelMock, null, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_ASK);
    }
    
    
    @Test
    public void realTickBidTypeTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        
        EasyMock.replay(bidPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,null, bidPriceLevelMock, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_BID);
    }
    
    @Test
    public void realTickBothTypesTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(237.8)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(5000));
        
        EasyMock.replay(bidPriceLevelMock);
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,askPriceLevelMock, bidPriceLevelMock, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_ASK);
    }
    
    
     @Test
    public void realTickBothTypesTest1() {
        int securityId=1000;
        Double displayFactor = 0.01;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(36925)));
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(36950)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(62));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(7));
        
        EasyMock.replay(bidPriceLevelMock);
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,askPriceLevelMock, bidPriceLevelMock, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 1, 369.25, 369.5, 369.5, 62, 6, 1, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_ASK);
    }
    
    
     @Test
    public void realTickAskChangeBidStaysTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(234.5)));
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(237.8)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(100));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(5000));
        
        EasyMock.replay(bidPriceLevelMock);
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,askPriceLevelMock, bidPriceLevelMock, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_ASK);
    }
    
        @Test
    public void realTickBidChangeAskStaysTest() {
        int securityId=1000;
         Double displayFactor = 1.0;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(235.0)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(120));
        
        EasyMock.replay(bidPriceLevelMock);
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,askPriceLevelMock, bidPriceLevelMock, System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120, 5, Utils.getDate(), TICK_SOURCE.CME);
        
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        
         assertEquals(type, Tick.ATTBB_TICKTYPE_BID);
    }
    
        
    @Test
    public void tradeTickTypeTest() {
        int securityId=1000;
        Double displayFactor = 0.001;
        
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, new ScaledDecimal(new BigDecimal(250)),500,System.currentTimeMillis());
        Tick tick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        int type = tickCache.getTickType(tick,tickDelta,displayFactor);
        assertEquals(type, Tick.ATTBB_TICKTYPE_TRADE);
    }
    
    @Test
    public void applyTradeDeltaOnRealTickTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, new ScaledDecimal(new BigDecimal(250)),500,System.currentTimeMillis());
        
        Tick masterTick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        Tick tick = tickCache.apply(tickDelta, masterTick, displayFactor);
        assertEquals("CLX5", tick.getSecurity());
        assertEquals(1, tick.getSequence());
        assertEquals(234.5, tick.getBidPrice());
        assertEquals(235.0, tick.getAskPrice());
        assertEquals(250.0, tick.getTradePrice());
        assertEquals(100, tick.getBidSize());
        assertEquals(120, tick.getAskSize());
        assertEquals(500, tick.getTradeSize());
        assertEquals(TICK_SOURCE.CME, tick.getTick_source());
       
       // assertEquals(type, Tick.ATTBB_TICKTYPE_TRADE);
    }
    
    
    @Test
    public void applyTradeDeltaOnSkeletonTickTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, new ScaledDecimal(new BigDecimal(250)),500,System.currentTimeMillis());
        
        Tick masterTick = creatTick("CLX5", 0, 0, 0, 0, 0, 0,0, Utils.getDate(), TICK_SOURCE.CME);
        Tick tick = tickCache.apply(tickDelta, masterTick, displayFactor);
        assertEquals("CLX5", tick.getSecurity());
        assertEquals(0, tick.getSequence());
        assertEquals(0.0, tick.getBidPrice());
        assertEquals(0.0, tick.getAskPrice());
        assertEquals(250.0, tick.getTradePrice());
        assertEquals(0, tick.getBidSize());
        assertEquals(0, tick.getAskSize());
        assertEquals(500, tick.getTradeSize());
        assertEquals(TICK_SOURCE.CME, tick.getTick_source());
       
       // assertEquals(type, Tick.ATTBB_TICKTYPE_TRADE);
    }
    
    
     @Test
    public void applyDummyDeltaOnRealTickTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId, null,null,System.currentTimeMillis());
        
        Tick masterTick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        Tick tick = tickCache.apply(tickDelta, masterTick, displayFactor);
        assertEquals("CLX5", tick.getSecurity());
        assertEquals(1, tick.getSequence());
        assertEquals(234.5, tick.getBidPrice());
        assertEquals(235.0, tick.getAskPrice());
        assertEquals(234.0, tick.getTradePrice());
        assertEquals(100, tick.getBidSize());
        assertEquals(120, tick.getAskSize());
        assertEquals(5, tick.getTradeSize());
        assertEquals(TICK_SOURCE.CME, tick.getTick_source());
       
       // assertEquals(type, Tick.ATTBB_TICKTYPE_TRADE);
    }
    
    
    @Test
    public void applyBidAskDummyDeltaOnSkeletonTickTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        
        EasyMock.replay(bidPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,null ,bidPriceLevelMock,System.currentTimeMillis());
        
        Tick masterTick = creatTick("CLX5", 0, 0, 0, 0, 0, 0,0, Utils.getDate(), TICK_SOURCE.CME);
        Tick tick = tickCache.apply(tickDelta, masterTick, displayFactor);
        assertEquals("CLX5", tick.getSecurity());
        assertEquals(0, tick.getSequence());
        assertEquals(238.0, tick.getBidPrice());
        assertEquals(0.0, tick.getAskPrice());
        assertEquals(0.0, tick.getTradePrice());
        assertEquals(1000, tick.getBidSize());
        assertEquals(0, tick.getAskSize());
        assertEquals(0, tick.getTradeSize());
        assertEquals(TICK_SOURCE.CME, tick.getTick_source());
       
       // assertEquals(type, Tick.ATTBB_TICKTYPE_TRADE);
    }
    
    
    @Test
    public void applyBidAskDummyDeltaOnRealTickTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        IPriceLevel bidPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(bidPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(bidPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        
        EasyMock.replay(bidPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,null ,bidPriceLevelMock,System.currentTimeMillis());
        
            Tick masterTick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        Tick tick = tickCache.apply(tickDelta, masterTick, displayFactor);
        assertEquals("CLX5", tick.getSecurity());
        assertEquals(1, tick.getSequence());
        assertEquals(238.0, tick.getBidPrice());
        assertEquals(235.0, tick.getAskPrice());
        assertEquals(234.0, tick.getTradePrice());
        assertEquals(1000, tick.getBidSize());
        assertEquals(120, tick.getAskSize());
        assertEquals(5, tick.getTradeSize());
        assertEquals(TICK_SOURCE.CME, tick.getTick_source());
    }
    
    
    @Test
    public void applyAskBidDummyDeltaOnSkeletonTickTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,askPriceLevelMock ,null,System.currentTimeMillis());
        
        Tick masterTick = creatTick("CLX5", 0, 0, 0, 0, 0, 0,0, Utils.getDate(), TICK_SOURCE.CME);
        Tick tick = tickCache.apply(tickDelta, masterTick, displayFactor);
        assertEquals("CLX5", tick.getSecurity());
        assertEquals(0, tick.getSequence());
        assertEquals(0.0, tick.getBidPrice());
        assertEquals(238.0, tick.getAskPrice());
        assertEquals(0.0, tick.getTradePrice());
        assertEquals(0, tick.getBidSize());
        assertEquals(1000, tick.getAskSize());
        assertEquals(0, tick.getTradeSize());
        assertEquals(TICK_SOURCE.CME, tick.getTick_source());
       
       // assertEquals(type, Tick.ATTBB_TICKTYPE_TRADE);
    }
    
    
    @Test
    public void applyAskBidDummyDeltaOnRealTickTest() {
        int securityId=1000;
        Double displayFactor = 1.0;
        IPriceLevel askPriceLevelMock = EasyMock.createMock(IPriceLevel.class);
        EasyMock.expect(askPriceLevelMock.getPrice()).andReturn(new ScaledDecimal(new BigDecimal(238)));
        EasyMock.expect(askPriceLevelMock.getQuantity()).andReturn(new Integer(1000));
        
        EasyMock.replay(askPriceLevelMock);
        TickCache tickCache = new TickCache();
        TickDelta tickDelta = new TickDelta(securityId,askPriceLevelMock,null,System.currentTimeMillis());
        
        Tick masterTick = creatTick("CLX5", 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        Tick tick = tickCache.apply(tickDelta, masterTick, displayFactor);
        assertEquals("CLX5", tick.getSecurity());
        assertEquals(1, tick.getSequence());
        assertEquals(234.5, tick.getBidPrice());
        assertEquals(238.0, tick.getAskPrice());
        assertEquals(234.0, tick.getTradePrice());
        assertEquals(100, tick.getBidSize());
        assertEquals(1000, tick.getAskSize());
        assertEquals(5, tick.getTradeSize());
        assertEquals(TICK_SOURCE.CME, tick.getTick_source());
    }
    
    
    @Test
    public void validateTest() {
                
        String ticker = "CLX5";
        TickCache tickCache = new TickCache();
        Tick masterTick = creatTick(ticker, 1, 234.5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_DUMMY);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertTrue(tickCache.validateTick(masterTick,ticker));
        masterTick.setType(Tick.ATTBB_TICKTYPE_BID);
        assertTrue(tickCache.validateTick(masterTick,ticker));
        masterTick.setType(Tick.ATTBB_TICKTYPE_TRADE);
        assertTrue(tickCache.validateTick(masterTick,ticker));
        
       
    }
    
    @Test
    public void validateTest1() {
                
        String ticker = "CLX5";
        TickCache tickCache = new TickCache();
        Tick masterTick = creatTick(ticker, 1, 0, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        masterTick = creatTick(ticker, 1, 243.0, 0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        masterTick = creatTick(ticker, 1, 243.0, 238.0, 0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        masterTick = creatTick(ticker, 1, 243.0, 238.0, 321.0, 0, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        masterTick = creatTick(ticker, 1, 243.0, 238.0, 321.0, 50,0 ,5, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        masterTick = creatTick(ticker, 1, 243.0, 238.0, 321.0, 50,123 ,0, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        
        masterTick = creatTick(ticker, 1, 0.0, 0.0, 358.3, 0 ,0 ,50, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        
        masterTick = creatTick(ticker, 1, 257.3, 269.8, 0.0, 130 ,50 ,0, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
       
    }
    
      @Test
    public void validateTest2() {
                
        String ticker = "CLX5";
        TickCache tickCache = new TickCache();
        Tick masterTick = creatTick(ticker, 1, -5, 235.0, 234.0, 100, 120,5, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
                
        masterTick = creatTick(ticker, 1, -10.0, -30.0, 358.3, 0 ,0 ,50, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
        
        masterTick = creatTick(ticker, 1, 257.3, 269.8, -480.0, 130 ,50 ,0, Utils.getDate(), TICK_SOURCE.CME);
        masterTick.setType(Tick.ATTBB_TICKTYPE_ASK);
        assertFalse(tickCache.validateTick(masterTick,ticker));
       
    }
    


    
    
    
    
    
    
    

    
}
