/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pelynch.cmefeed;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tom
 */

public class CmeFeedUtilsTest {
    
    private CmeFeedUtils cmeFeedUtils;
    
    
  
    @Before
    public void setUp() {
       cmeFeedUtils = new CmeFeedUtils();
    }
    
    @After
    public void tearDown() {
    }

   

    /**
     * Test of parseSymbol method, of class SubscribeRequestManager.
     */
    @Test
    public void testNullArgParseSymbol() {
        
        String symbol = null;
        String expResult = null;
       
        String result = cmeFeedUtils.parseSymbol(symbol);
        assertEquals("NULL ARG TEST",expResult, result);
        
       
      
    }
    
    @Test
    public void testManySpacesParseSymbol() {
        String symbol = "ESH5     Comdty";
        String expResult = "ESH5";
        
        String result = cmeFeedUtils.parseSymbol(symbol);
        assertEquals("MANY SPACES TEST 1",expResult, result);
        
        symbol = "   ESH5     Comdty   ";
        expResult = "ESH5";
        result = cmeFeedUtils.parseSymbol(symbol);
        assertEquals("MANY SPACES TEST 2",expResult, result);
    }
    
    
    @Test
    public void testInvalidArgParseSymbol() {
        String symbol = "ESH5Comdty";
        String expResult = null;
        
        String result = cmeFeedUtils.parseSymbol(symbol);
        assertEquals("INVALID ARG",expResult, result);
        
        
    }
    
    
    

    /**
     * Test of constructUid method, of class SubscribeRequestManager.
     */
   
    
}
